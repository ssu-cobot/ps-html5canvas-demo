module Main where

import Prelude

import Data.Array ((!!), length, updateAtIndices)
import Data.FoldableWithIndex (traverseWithIndex_)
import Data.Int (toNumber)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Math (atan2, tau)
import Partial.Unsafe (unsafePartial)

import Graphics.Canvas (CanvasImageSource, Context2D,
                        drawImageFull, getCanvasElementById, getContext2D,
                        restore, rotate, save, translate, tryLoadImage)
import Signal (foldp, runSignal, sampleOn)
import Signal.DOM (Touch, animationFrame, keyPressed, touch)

main :: Effect Unit
main = tryLoadImage "../assets/images/tilesheet.png" game

game :: Maybe CanvasImageSource -> Effect Unit
game Nothing = log "failed to load tilesheet"
game (Just tilesheet) = do
  log "tilesheet loaded"
  frame <- animationFrame
  left  <- keyPressed 0x25
  up    <- keyPressed 0x26
  right <- keyPressed 0x27
  down  <- keyPressed 0x28
  touch <- touch
  let input = { left: _, up: _, right: _, down: _, touch: _ }
            <$> left <*> up <*> right <*> down <*> touch
  let state = foldp update (initialState tilesheet) (sampleOn frame input)
  runSignal (render <$> state)

type GameState =
  { context   :: Context2D
  , tilesheet :: CanvasImageSource
  , playfield :: Playfield
  , player    :: PlayerState
  }

type PlayerState =
  { position :: Point
  , rotation :: Number
  }

initialState :: CanvasImageSource -> Effect GameState
initialState tilesheet = unsafePartial do
  Just canvas <- getCanvasElementById "canvas"
  context <- getContext2D canvas
  pure { context, tilesheet
       , playfield: initialPlayfield
       , player: { position: { x: middle, y: middle }
                 , rotation: 0.0
                 }
       }
  where middle = tilesize * (toNumber $ playfieldSize - 1) / 2.0
        playfieldSize = length initialPlayfield

type Input =
  { left  :: Boolean
  , up    :: Boolean
  , right :: Boolean
  , down  :: Boolean
  , touch :: Array Touch
  }

update :: Input -> Effect GameState -> Effect GameState
update input state = do
  { context, tilesheet, playfield, player } <- state
  let player' = updatePlayer player input
  pure { context, tilesheet, playfield , player: player' }

updatePlayer :: PlayerState -> Input -> PlayerState
updatePlayer player input = { position: updatePosition player.position input
                            , rotation: updateRotation player.rotation input
                            }

updatePosition :: Point -> Input -> Point
updatePosition { x, y } { left, up, right, down, touch: [] } = { x: x', y: y' }
  where x' = x + case { left, right } of
          { left: true, right: false } -> -dx
          { left: false, right: true } ->  dx
          otherwise -> 0.0
        y' = y + case { up, down } of
          { up: true, down: false } -> -dy
          { up: false, down: true } ->  dy
          otherwise -> 0.0
        dx = 1.0
        dy = 1.0
updatePosition { x, y } { touch } = { x: toNumber t0.pageX
                                    , y: toNumber t0.pageY
                                    }
  where t0 = unsafePartial $ case touch !! 0 of Just t -> t

updateRotation :: Number -> Input -> Number
updateRotation θ { left: false, up: false, right: false, down: false } = θ
updateRotation _ { left, up, right, down } = targetθ
  where
    targetX = case { left, right } of
      { left: true, right: false } -> -1.0
      { left: false, right: true } ->  1.0
      otherwise -> 0.0
    targetY = case { up, down } of
      { up: true, down: false } ->  1.0
      { up: false, down: true } -> -1.0
      otherwise -> 0.0
    -- XXX: x and y axis swapped in call to vectorAngle so "up" is 0 radians
    targetθ = vectorAngle targetY targetX
    -- TODO: Determine the shortest rotation needed to reach the target angle
    -- from the current angle.

vectorAngle :: Number -> Number -> Number
vectorAngle 0.0 0.0 = 0.0
vectorAngle 0.0 y = tau * if y > 0.0 then 0.25 else 0.75
vectorAngle x y = atan2 y x

updatePlayfield :: Playfield -> Int -> Int -> Tile -> Partial => Playfield
updatePlayfield playfield row col tile = do
  let Just r = playfield !! row
  let row' = updateAtIndices [Tuple col tile] r
  updateAtIndices [Tuple row row'] playfield

render :: Effect GameState -> Effect Unit
render state = do
  { context, tilesheet, playfield, player } <- state
  renderPlayfield context tilesheet playfield
  save context
  let offset = tilesize / 2.0
  let position = { translateX: player.position.x + offset
                 , translateY: player.position.y + offset
                 }
  translate context position
  rotate context player.rotation
  drawTile context tilesheet (tilemap $ Tank Green 0) { x: -offset, y: -offset }
  restore context

data TankColor = Green | Blue
derive instance eqTankColor :: Eq TankColor

data Tile = Road
          | Tank TankColor Int
          | Explosion Int
          | Ammo
          | Projectile
          | Life
          | Powerup
          | Wall Int
derive instance eqTile :: Eq Tile

type Point = { x :: Number, y :: Number }

tilemap' :: Int -> Point
tilemap' i =
  { x: tilesize * toNumber (i `mod` sheetwidth)
  , y: tilesize * toNumber (i `div` sheetwidth)
  }

tilemap :: Tile -> Point
tilemap Road = tilemap' 0
tilemap (Tank Green i) = tilemap' (1 + i)
tilemap (Tank Blue i) = tilemap' (9 + i)
tilemap (Explosion i) = tilemap' (2 * 8 + 1 + i)
tilemap Ammo = tilemap' (2 * 8 + 4)
tilemap Projectile = tilemap' (2 * 8 + 5)
tilemap Life = tilemap' (2 * 8 + 6)
tilemap Powerup = tilemap' (2 * 8 + 7)
tilemap (Wall i) = tilemap' (3 * 8 + i)

tilesize :: Number
tilesize = 32.0

sheetwidth :: Int
sheetwidth = 8

type Playfield = Array (Array Tile)

-- TODO: This should be procedurally generated at run time.
initialPlayfield :: Playfield
initialPlayfield =
  [ [pu, rd, rd, rd, rd, w6, rd, w7, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, w4, rd, b7, b6, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, am, w2, rd, e0, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, e1, w0, w1, rd, rd, b7, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, pj, rd, rd, rd, w3, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, g1, rd, rd, rd, g5, rd, w5, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [b0, rd, rd, w0, w1, rd, lf, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, e2, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  , [rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd, rd]
  ]
  where
    rd = Road
    b0 = Tank Blue 0
    b1 = Tank Blue 1
    b2 = Tank Blue 2
    b3 = Tank Blue 3
    b4 = Tank Blue 4
    b5 = Tank Blue 5
    b6 = Tank Blue 6
    b7 = Tank Blue 7
    g0 = Tank Green 0
    g1 = Tank Green 1
    g2 = Tank Green 2
    g3 = Tank Green 3
    g4 = Tank Green 4
    g5 = Tank Green 5
    g6 = Tank Green 6
    g7 = Tank Green 7
    e0 = Explosion 0
    e1 = Explosion 1
    e2 = Explosion 2
    am = Ammo
    pj = Projectile
    lf = Life
    pu = Powerup
    w0 = Wall 0
    w1 = Wall 1
    w2 = Wall 2
    w3 = Wall 3
    w4 = Wall 4
    w5 = Wall 5
    w6 = Wall 6
    w7 = Wall 7

drawTile :: Context2D -> CanvasImageSource -> Point -> Point -> Effect Unit
drawTile ctx sheet src dst =
  drawImageFull ctx sheet src.x src.y tilesize tilesize dst.x dst.y tilesize tilesize

renderTile :: Context2D -> CanvasImageSource -> Tile -> Point -> Effect Unit
renderTile ctx sheet tile location = do
  draw Road
  unless (tile == Road) $
    draw tile
  where
    draw t = drawTile ctx sheet (tilemap t) location

renderPlayfield :: Context2D -> CanvasImageSource -> Playfield -> Effect Unit
renderPlayfield ctx sheet = traverseWithIndex_ renderPlayfieldRow
  where renderPlayfieldRow row = traverseWithIndex_ renderPlayfieldTile
          where renderPlayfieldTile col tile = renderTile ctx sheet tile location
                  where location = { x: tilesize * toNumber col
                                   , y: tilesize * toNumber row
                                   }
