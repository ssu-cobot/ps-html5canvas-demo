PureScript HTML5 Canvas Tile-Based Rendering Demo
=================================================

A quick and dirty demo for rendering a tile-based game with HTML5 canvas.

Prerequisites
-------------

[NPM][] is required to install the dependencies for this project.

[NPM]: https://www.npmjs.com/get-npm


Dependencies
------------

This project is written in [PureScript][] and uses [Pulp][] to build and
[Bower][] to install libraries.

[PureScript]: http://www.purescript.org
[Pulp]: https://github.com/purescript-contrib/pulp
[Bower]: https://bower.io

To install PureScript, Pulp, and Bower, run:
```
npm install -g purescript pulp bower
```

Then, clone the repo and fetch the library depencencies of this project:
```
git clone https://gitlab.com/ssu-cobot/ps-html5canvas-demo
cd ps-html5canvas-demo
bower install
```

Usage
-----

To start a development server that will compile the sources as needed:
```
pulp server
```

Then navigate in your browser to http://localhost:1337/html to see the page.
